#!/bin/bash

user=""
ssh_key=""

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

apt update -y
apt upgrade -y
apt install -y git vim ncdu exa bat gpg curl

useradd -m $user
chsh -s /bin/bash $user
cd /tmp
git clone https://codeberg.org/nor/serverfiles
cd serverfiles

cp sshd_config /etc/ssh/sshd_config
cp sudoers /etc/sudoers
cp .bashrc /home/$user/.bashrc
cp .bashrc /root/.bashrc

mkdir -p /etc/ssh/authorized_keys/
echo $ssh_key > /etc/ssh/authorized_keys/$user

echo "$user ALL=(ALL) ALL" >> /etc/sudoers.d/80-$user-user